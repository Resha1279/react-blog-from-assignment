import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./components/Login";
import Signup from "./components/Signup";
import BlogList from "./components/blog/BlogList";
import Notfound from "./ui/Notfound";
import Navbar from "./navigation/Navbar";
import AuthProvider from "./context/AuthProvider";
import BlogProvider from "./context/BlogProvider";
function App() {
  return (
    <>
      <Router>
        <AuthProvider>
          <BlogProvider>
            <Navbar />
            <Switch>
              <Route exact path="/">
                <Login />
              </Route>
              <Route path="/signup">
                <Signup />
              </Route>
              <Route path="/posts">
                <BlogList />
              </Route>
              <Route path="*">
                <Notfound />
              </Route>
            </Switch>
          </BlogProvider>
        </AuthProvider>
      </Router>
    </>
  );
}

export default App;
