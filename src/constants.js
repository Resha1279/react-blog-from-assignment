export const API_KEY =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTYzNTM5NTU2MywiZXhwIjoxOTUwOTcxNTYzfQ.OmGhNW11V2cwQerXEY8eIlxsCVjRrKYeDF7Qki3g2b4";

export const BASE_URL = "https://unvcbtiihieuqvojaqos.supabase.co";

export const AUTHENTICATION_TOKEN = "token";

//actions--auth
export const SET_TOKEN = "SET_TOKEN";
export const REMOVE_TOKEN = "REMOVE_TOKEN";

//actions--blog
export const FETCH_BLOG = "FETCH_BLOG";
export const DELETE_BLOG = "DELETE_BLOG";
export const SHOW_FORM = "SHOW_FORM";
export const IS_EDIT = "IS_EDIT";
export const IS_LOADING = "IS_LOADING";
