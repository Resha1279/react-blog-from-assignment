import {
  IS_EDIT,
  DELETE_BLOG,
  FETCH_BLOG,
  SHOW_FORM,
  IS_LOADING,
} from "../constants";

export const blogReducer = (state, action) => {
  switch (action.type) {
    case FETCH_BLOG:
      return {
        ...state,
        blogs: action.payload.sort((a, b) => {
          if (a.position < b.position) {
            return -1;
          }
          if (a.position > b.position) {
            return 1;
          }
          return 0;
        }),
        isLoading: false,
      };
    case DELETE_BLOG:
      return {
        ...state,
        blogs: state.blogs.filter((blog) => {
          return blog.id !== action.payload;
        }),
      };
    case SHOW_FORM:
      return { ...state, showForm: action.payload };
    case IS_EDIT:
      return {
        ...state,
        isEdit: action.payload,
      };
    case IS_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };
    default:
      return state;
  }
};
