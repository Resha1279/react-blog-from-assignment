import React, { useReducer } from "react";
import AuthContext from "./auth-context";
import { authReducer } from "../reducers/authReducer";
import { REMOVE_TOKEN, SET_TOKEN } from "../constants";

const AuthProvider = (props) => {
  const defaultState = {
    isAuth: false,
    token: "",
  };

  const [state, dispatch] = useReducer(authReducer, defaultState);
  const setToken = (payload) => {
    dispatch({
      type: SET_TOKEN,
      payload: payload,
    });
  };
  const removeToken = () => {
    dispatch({
      type: REMOVE_TOKEN,
    });
  };
  return (
    <AuthContext.Provider
      value={{
        isAuth: state.isAuth,
        token: state.token,
        setToken,
        removeToken,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};
export default AuthProvider;
