import React, { useReducer, useContext } from "react";
import BlogContext from "./blog-context";
import { blogReducer } from "../reducers/blogReducer";
import authContext from "./auth-context";
import {
  API_KEY,
  BASE_URL,
  DELETE_BLOG,
  FETCH_BLOG,
  SHOW_FORM,
  IS_EDIT,
  IS_LOADING,
} from "../constants";

const BlogProvider = (props) => {
  const { token } = useContext(authContext);

  const defaultState = {
    blogs: [],
    isLoading: false,
    showForm: false,
    isEdit: false,
  };

  const [state, dispatch] = useReducer(blogReducer, defaultState);

  const setShowForm = (payload) => {
    dispatch({
      type: SHOW_FORM,
      payload: payload,
    });
  };

  const setIsEdit = (payload) => {
    dispatch({
      type: IS_EDIT,
      payload: payload,
    });
  };

  const setIsLoading = (payload) => {
    dispatch({
      type: IS_LOADING,
      payload: payload,
    });
  };

  const fetchBlogs = () => {
    const url = `${BASE_URL}/rest/v1/post`;

    let header = {
      apikey: API_KEY,
      Authorization: `Bearer ${token}`,
    };

    fetch(url, {
      method: "GET",
      headers: header,
    })
      .then((response) => {
        console.log(response.status);
        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((data) => {
        console.log("getpost Success:", data);
        dispatch({
          type: FETCH_BLOG,
          payload: data,
        });
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  const deleteBlog = (id) => {
    const url = `${BASE_URL}/rest/v1/post?id=eq.${id}`;

    let header = {
      apikey: API_KEY,
      Authorization: `Bearer ${token}`,
    };

    fetch(url, {
      method: "DELETE",
      headers: header,
    })
      .then((response) => {
        console.log(response.status);
        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        } else {
          dispatch({
            type: DELETE_BLOG,
            payload: id,
          });
        }
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  const addBlogs = ({ blogName, description, position, imageUrl }) => {
    const url = `${BASE_URL}/rest/v1/post`;

    let header = {
      "Content-Type": "application/json",
      apikey: API_KEY,
      Authorization: `Bearer ${token}`,
      Prefer: "return=representation",
    };

    const body = {
      name: blogName.trim(),
      description: description.trim(),
      position,
      image_url: imageUrl.trim(),
    };

    fetch(url, {
      method: "POST",
      headers: header,
      body: JSON.stringify(body),
    })
      .then((response) => {
        console.log(response.status);
        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        } else {
          fetchBlogs();
          setShowForm(false);
        }
      })
      .catch((error) => {
        console.error("Error:", error);
        alert("Blog was not added. Please check your connection and try again");
      });
  };

  const editBlog = ({ blogName, description, position, imageUrl, editId }) => {
    console.log(editId);
    setIsEdit(false);
    const url = `${BASE_URL}/rest/v1/post?id=eq.${editId}`;

    let header = {
      "Content-Type": "application/json",
      apikey: API_KEY,
      Authorization: `Bearer ${token}`,
      Prefer: "return=representation",
    };

    const body = {
      name: blogName.trim(),
      description: description.trim(),
      position,
      image_url: imageUrl.trim(),
    };

    fetch(url, {
      method: "PATCH",
      headers: header,
      body: JSON.stringify(body),
    })
      .then((response) => {
        console.log(response.status);
        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        } else {
          setShowForm(false);
          fetchBlogs();
        }
      })
      .catch((error) => {
        console.error("Error:", error);
        alert("Blog was not added. Please check your connection and try again");
        setShowForm(false);
        fetchBlogs();
      });
  };

  return (
    <BlogContext.Provider
      value={{
        blogs: state.blogs,
        showForm: state.showForm,
        isEdit: state.isEdit,
        addBlogs,
        deleteBlog,
        setShowForm,
        setIsEdit,
        editBlog,
        fetchBlogs,
        setIsLoading,
      }}
    >
      {props.children}
    </BlogContext.Provider>
  );
};

export default BlogProvider;
