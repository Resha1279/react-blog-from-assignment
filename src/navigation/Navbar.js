import { AppBar, Box, Button, Grid, Typography } from "@mui/material";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import React, { useContext, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Logout from "../components/Logout";
import authContext from "../context/auth-context";
import AddBlog from "../components/blog/AddBlog";
import blogContext from "../context/blog-context";

export default function Navbar() {
  const [isLogout, setIsLogout] = useState(false);
  const { isAuth } = useContext(authContext);
  const { addBlogs, showForm, setShowForm, setIsEdit } =
    useContext(blogContext);

  useEffect(() => {
    setIsLogout(false);
  }, [isAuth]);

  const hideForm = () => {
    setShowForm(false);
  };
  const hideLogout = () => {
    setIsLogout(false);
  };

  return (
    <>
      <AppBar position="sticky" sx={{ p: 1 }}>
        <section className="container">
          <Grid
            container
            direction="row"
            justifyContent="space-between"
            alignItems="center"
          >
            <Grid
              container
              direction="row"
              justifyContent="space-between"
              alignItems="center"
              item
              xs={6}
            >
              <Typography variant="h6">
                <MenuBookIcon />
                BLOG
              </Typography>
            </Grid>
            <Grid item xs={6} container justifyContent="flex-end">
              {isAuth ? (
                <>
                  <Button
                    variant="outlined"
                    style={{ color: "#FFFFFF" }}
                    onClick={() => {
                      setIsEdit(false);
                      setShowForm(true);
                    }}
                  >
                    Add Blog
                  </Button>
                  <Button
                    variant="outlined"
                    style={{ color: "#FFFFFF" }}
                    onClick={() => {
                      setIsLogout(true);
                    }}
                  >
                    Logout
                  </Button>
                </>
              ) : (
                <>
                  <Link to="/">
                    <Typography
                      variant="button"
                      style={{ marginRight: "18px" }}
                      color="common.white"
                    >
                      Login
                    </Typography>
                  </Link>

                  <Link to="/signup">
                    <Typography variant="button" color="common.white">
                      Sign Up
                    </Typography>
                  </Link>
                </>
              )}
            </Grid>
          </Grid>
        </section>
      </AppBar>
      {isLogout && <Logout hideLogout={hideLogout} />}
      {showForm && <AddBlog hideForm={hideForm} addBlogs={addBlogs} />}
    </>
  );
}
