import React, { useState } from "react";
import {
  Card,
  CardMedia,
  CardContent,
  Typography,
  CardActions,
  Button,
} from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import Moment from "react-moment";

export const BlogItem = ({
  id,
  name,
  description,
  updated_at,
  image_url,
  deleteBlog,
  setIsEdit,
}) => {
  const [btnLoading, setBtnLoading] = useState(false);
  const [readMore, setReadMore] = useState(false);
  return (
    <Card sx={{ maxWidth: 800 }} style={{ margin: "20px auto" }}>
      <CardMedia
        component="img"
        height="400"
        image={image_url}
        alt="blog"
        onError={(e) => {
          e.target.onerror = null;
          e.target.src =
            "https://timesofindia.indiatimes.com/photo/67586673.cms";
        }}
      />
      <CardContent>
        <Typography variant="h5" component="div">
          {name}
        </Typography>
        <Typography variant="body2" style={{ marginBottom: "10px" }}>
          <Moment style={{ color: "#d62cb2" }} fromNow>
            {updated_at}
          </Moment>
        </Typography>

        <Typography variant="body2" color="text.secondary">
          {readMore ? description : `${description.substring(0, 320)}...`}
          <Button
            size="small"
            onClick={() => {
              setReadMore(!readMore);
            }}
          >
            {readMore ? "Show Less" : "read more"}
          </Button>
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          size="small"
          onClick={() => {
            setIsEdit(id);
          }}
        >
          Edit
        </Button>
        <LoadingButton
          loading={btnLoading}
          size="small"
          onClick={() => {
            setBtnLoading(true);
            deleteBlog(id);
          }}
        >
          Delete
        </LoadingButton>
      </CardActions>
    </Card>
  );
};
