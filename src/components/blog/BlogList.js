import React, { useState, useContext, useEffect } from "react";
import authContext from "../../context/auth-context";
import AddBlog from "./AddBlog";
import { BlogItem } from "./BlogItem";
import { useHistory } from "react-router-dom";
import blogContext from "../../context/blog-context";

export default function BlogList() {
  const [toEditBlog, setToEditBlog] = useState([]);

  const { isAuth } = useContext(authContext);
  const {
    blogs,
    showForm,
    isEdit,
    addBlogs,
    setShowForm,
    setIsEdit,
    fetchBlogs,
    deleteBlog,
    editBlog,
    setIsLoading,
    isLoading,
  } = useContext(blogContext);
  const history = useHistory();

  useEffect(() => {
    if (isAuth) {
      setIsLoading(true);
      fetchBlogs();
    } else {
      history.replace("/");
    }
  }, []);

  const hideForm = () => {
    setShowForm(false);
    setIsEdit(false);
  };

  //to pre-populate the form for editing
  const editForm = (id) => {
    setIsEdit(true);
    setShowForm(true);
    let blog = blogs.filter((blog) => blog.id === id);
    setToEditBlog(...blog);
  };

  return (
    <div>
      {showForm && (
        <AddBlog
          addBlogs={addBlogs}
          blog={toEditBlog}
          hideForm={hideForm}
          editBlog={editBlog}
          isEdit={isEdit}
        />
      )}
      {isLoading ? (
        <div>
          <p>Loading....</p>
        </div>
      ) : (
        blogs.map((blog) => {
          return (
            <BlogItem
              key={blog.id}
              {...blog}
              deleteBlog={deleteBlog}
              setIsEdit={editForm}
            />
          );
        })
      )}
    </div>
  );
}
