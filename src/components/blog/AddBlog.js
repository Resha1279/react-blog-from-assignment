import { Modal, TextField, Button, Box } from "@mui/material";
import React from "react";
import { useState } from "react";
import LoadingButton from "@mui/lab/LoadingButton";

export default function AddBlog({
  addBlogs,
  hideForm,
  isEdit,
  editBlog,
  blog,
}) {
  const [blogName, setBlogName] = useState(isEdit ? blog.name : "");
  const [description, setDescription] = useState(
    isEdit ? blog.description : ""
  );
  const [position, setPosition] = useState(isEdit ? blog.position : "");
  const [imageUrl, setImageUrl] = useState(isEdit ? blog.image_url : "");

  const [btnLoading, setBtnLoading] = useState(false);

  const boxStyle = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    boxShadow: 24,
    p: 4,
  };
  return (
    <Modal open={true}>
      <Box sx={boxStyle}>
        <form
          action="submit"
          onSubmit={(e) => {
            e.preventDefault();
            setBtnLoading(true);
            if (isEdit) {
              editBlog({
                blogName,
                description,
                position,
                imageUrl,
                editId: blog.id,
              });
            } else {
              addBlogs({
                blogName,
                description,
                position,
                imageUrl,
              });
            }
          }}
        >
          <TextField
            label="Name"
            type="text"
            placeholder="Enter Name"
            required
            value={blogName}
            onChange={(e) => {
              setBlogName(e.target.value);
            }}
            sx={{ width: "90%", margin: "10px" }}
          />
          <TextField
            label="Description"
            type="text"
            placeholder="Enter description"
            required
            value={description}
            onChange={(e) => {
              setDescription(e.target.value);
            }}
            sx={{ width: "90%", margin: "10px" }}
          />
          <TextField
            label="Position"
            type="number"
            placeholder="Enter position"
            required
            value={position}
            onChange={(e) => {
              setPosition(e.target.value);
            }}
            sx={{ width: "90%", margin: "10px" }}
          />
          <TextField
            label="Image URL"
            type="text"
            placeholder="Enter URL"
            required
            value={imageUrl}
            onChange={(e) => {
              setImageUrl(e.target.value);
            }}
            sx={{ width: "90%", margin: "10px" }}
          />
          <Button
            type="button"
            sx={{ width: "50%" }}
            onClick={() => {
              hideForm();
            }}
          >
            Cancel
          </Button>
          <LoadingButton
            loading={btnLoading}
            type="submit"
            sx={{ width: "50%" }}
          >
            Done
          </LoadingButton>
        </form>
      </Box>
    </Modal>
  );
}
