import React, { useState, useContext, useEffect } from "react";
import { BASE_URL, AUTHENTICATION_TOKEN, API_KEY } from "../constants";
import authContext from "../context/auth-context";
import { useHistory } from "react-router-dom";
import {
  Avatar,
  Grid,
  Paper,
  TextField,
  Typography,
  Button,
} from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [invalidText, setInvalidText] = useState("");

  const { setToken } = useContext(authContext);

  const history = useHistory();

  useEffect(() => {
    const token = JSON.parse(window.localStorage.getItem(AUTHENTICATION_TOKEN));
    if (token) {
      setToken(token);
      history.replace("/posts");
    }
  });

  const submitInputs = () => {
    const url = `${BASE_URL}/auth/v1/token?grant_type=password`;

    const body = {
      email: email.trim(),
      password: password.trim(),
    };

    let header = {
      "Content-Type": "application/json",
      apikey: API_KEY,
    };

    fetch(url, {
      method: "POST",
      headers: header,
      body: JSON.stringify(body),
    })
      .then((response) => {
        console.log(response.status);
        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((data) => {
        console.log("login Success:", data);
        console.log("token", data.access_token);
        window.localStorage.clear();
        window.localStorage.setItem(
          AUTHENTICATION_TOKEN,
          JSON.stringify(data.access_token)
        );
        setToken(data.access_token);
        setIsLoading(false);
        history.replace("/posts");
      })
      .catch((error) => {
        setIsLoading(false);
        console.error("Error:", error);
        setInvalidText("Invalid email or password");
      });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (email !== "" && password !== "") {
      setIsLoading(true);
      submitInputs();
    }
  };

  const paperStyle = {
    padding: 20,
    width: 400,
    margin: "20px auto",
  };

  return (
    <div className="container">
      <Grid align="center" mt={8}>
        <Paper elevation={3} style={paperStyle}>
          <Avatar>
            <LockOutlinedIcon />
          </Avatar>
          <Typography m={2} variant="h5">
            Login
          </Typography>
          <Typography m={2} color="error">
            {invalidText}
          </Typography>
          <form action="submit" onSubmit={handleSubmit}>
            <TextField
              label="Email"
              type="email"
              placeholder="Enter Email"
              required
              value={email}
              onChange={(e) => {
                setEmail(e.target.value);
              }}
              sx={{ width: "90%", margin: "10px" }}
            />
            <TextField
              label="Password"
              type="password"
              required
              placeholder="Enter Password"
              sx={{ width: "90%", margin: "10px" }}
              value={password}
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />
            <LoadingButton
              loading={isLoading}
              type="submit"
              variant="contained"
              sx={{ width: "90%", margin: "10px" }}
            >
              Login
            </LoadingButton>
          </form>
          <Typography>or</Typography>
          <Button
            onClick={() => {
              history.replace("/signup");
            }}
          >
            Create new account
          </Button>
        </Paper>
      </Grid>
    </div>
  );
}
