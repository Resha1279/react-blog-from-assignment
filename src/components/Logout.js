import React, { useState, useEffect, useContext } from "react";
import authContext from "../context/auth-context";
import { API_KEY, BASE_URL } from "../constants";
import { useHistory } from "react-router-dom";

export default function Logout({ hideLogout }) {
  const [isLoading, setIsLoading] = useState(false);

  const { token, removeToken } = useContext(authContext);

  useEffect(() => {
    setIsLoading(true);
    logout();
  }, []);

  const history = useHistory();

  const logout = () => {
    const url = `${BASE_URL}/auth/v1/logout`;

    let header = {
      apikey: API_KEY,
      Authorization: `Bearer ${token}`,
    };

    fetch(url, {
      method: "POST",
      headers: header,
    })
      .then((response) => {
        console.log(response.status);
        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.text();
      })
      .then((data) => {
        console.log("logout Success:", data);

        window.localStorage.clear();
        removeToken();
        setIsLoading(false);
        history.replace("/");
      })
      .catch((error) => {
        setIsLoading(false);
        console.error("Error:", error);
        hideLogout();
        history.replace("/posts");
      });
  };

  return (
    <>
      {isLoading ? (
        <div>
          <p>Logging out...</p>
        </div>
      ) : null}
    </>
  );
}
