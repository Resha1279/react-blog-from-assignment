import React, { useState, useEffect } from "react";
import {
  Avatar,
  FormControl,
  Button,
  Grid,
  Paper,
  TextField,
  Typography,
} from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import { useHistory } from "react-router-dom";

import { BASE_URL, API_KEY } from "../constants";

export default function Signup() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [rePassword, setRePassword] = useState("");
  const [isEmailValid, setIsEmailValid] = useState(false);
  const [isPasswordValid, setIsPasswordValid] = useState(false);
  const [isPasswordMatch, setIsPasswordMatch] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  //for textfield only
  const [emailError, setEmailError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [passwordMatchError, setPasswordMatchError] = useState(false);
  const [emailErrorText, setEmailErrorText] = useState("");
  const [passwordErrorText, setPasswordErrorText] = useState("");
  const [passwordMatchErrorText, setPasswordMatchErrorText] = useState("");

  useEffect(() => {
    validateEmail();
  }, [email]);

  useEffect(() => {
    validatePassword();
  }, [password]);

  useEffect(() => {
    validateRePassword();
  }, [rePassword]);

  const history = useHistory();

  const validateEmail = () => {
    let expression = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email.match(expression)) {
      setIsEmailValid(true);
      setEmailErrorText(
        "A verification link will be sent to this email address"
      );
    } else {
      setIsEmailValid(false);
      if (email.length === 0) {
        setEmailErrorText("This field cannot be empty");
      } else {
        setEmailErrorText("Email invalid");
      }
    }
  };
  const validatePassword = () => {
    let expression =
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,30}$/;
    if (password.match(expression)) {
      setIsPasswordValid(true);
      setPasswordErrorText("");
    } else {
      setIsPasswordValid(false);
      if (password.length === 0) {
        setPasswordErrorText("This field cannot be empty");
      } else if (password.length < 6) {
        setPasswordErrorText("Weak");
      } else {
        setPasswordErrorText(
          "Password must include at least an uppercase, a lowercase, a number and a special character"
        );
      }
    }
  };
  const validateRePassword = () => {
    if (password === rePassword && rePassword.length > 0) {
      setIsPasswordMatch(true);
      setPasswordMatchErrorText("Match");
    } else {
      setIsPasswordMatch(false);
      if (rePassword.length === 0) {
        setPasswordMatchErrorText("This field cannot be empty");
      } else {
        setPasswordMatchErrorText("No match");
      }
    }
  };

  const submitInputs = () => {
    const url = `${BASE_URL}/auth/v1/signup`;

    const body = {
      email: email.trim(),
      password: password.trim(),
    };

    let header = {
      "Content-Type": "application/json",
      apikey: API_KEY,
    };

    fetch(url, {
      method: "POST",
      headers: header,
      body: JSON.stringify(body),
    })
      .then((response) => {
        console.log(response.status);
        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((data) => {
        console.log("Success:", data);
        setIsLoading(false);
        alert("Signup success. Please check your email and proceed to login");
        history.replace("/login");
      })
      .catch((error) => {
        setIsLoading(false);
        console.error("Error:", error);
      });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setIsLoading(true);
    if (isEmailValid && isPasswordValid && isPasswordMatch) {
      submitInputs();
    } else {
      setIsLoading(false);
    }
  };
  return (
    <div className="container">
      <Grid align="center" mt={6}>
        <Paper elevation={3} sx={{ padding: 3, width: 400 }}>
          <Avatar>
            <LockOutlinedIcon />
          </Avatar>
          <Typography m={2} variant="h5">
            Sign Up
          </Typography>
          <form
            noValidate
            autoComplete="off"
            action="submit"
            onSubmit={handleSubmit}
          >
            <FormControl sx={{ width: "90%", margin: "10px" }}>
              <TextField
                label="Email"
                type="email"
                placeholder="Enter Email"
                required
                error={emailError && !isEmailValid}
                helperText={emailError ? emailErrorText : ""}
                value={email}
                onChange={(e) => {
                  setEmailError(true);
                  setEmail(e.target.value);
                }}
              />
            </FormControl>
            <FormControl sx={{ width: "90%", margin: "10px" }}>
              <TextField
                label="Password"
                type="password"
                required
                error={passwordError && !isPasswordValid}
                helperText={passwordError ? passwordErrorText : ""}
                placeholder="Enter Password"
                value={password}
                onChange={(e) => {
                  setPasswordError(true);
                  setPassword(e.target.value);
                }}
              />
            </FormControl>
            <FormControl sx={{ width: "90%", margin: "10px" }}>
              <TextField
                label="Confirm Password"
                type="password"
                required
                error={
                  passwordMatchError === false
                    ? passwordMatchError
                    : !isPasswordMatch
                }
                helperText={passwordMatchError ? passwordMatchErrorText : ""}
                placeholder="Re-Enter Password"
                value={rePassword}
                onChange={(e) => {
                  setPasswordMatchError(true);
                  setRePassword(e.target.value);
                }}
              />
            </FormControl>
            <LoadingButton
              loading={isLoading}
              type="submit"
              variant="contained"
              sx={{ width: "90%", margin: "10px" }}
            >
              Sign Up
            </LoadingButton>
          </form>
          <Typography>Already have an account?</Typography>
          <Button
            onClick={() => {
              history.replace("/");
            }}
          >
            Login
          </Button>
        </Paper>
      </Grid>
    </div>
  );
}
